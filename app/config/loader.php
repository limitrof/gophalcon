<?php

require_once __DIR__ . "/../../vendor/autoload.php";
$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */

define('APP_PATH', BASE_PATH . '/app');

// Register some classes
/*$loader->registerClasses(
    [
        'CityService'         => 'library/CityService.php',
    ]
);*/

$loader->registerDirs(
    [
        APP_PATH . '/controllers/',
        APP_PATH . '/models/',
        APP_PATH . '/library/',
// NOT work with php -S 0.0.0.0:8080 -t public index.php
// php -S localhost:8000 -t public .htrouter.php
//        $config->application->controllersDir,
//        $config->application->modelsDir
    ]
);

$loader->register();



