<?php

class CityService
{
    /**
     * @var
     */
    private $url='https://api.openweathermap.org/data/2.5/weather';// todo move to config

    /**
     * ControllerBase constructor.
     * @param $weatherServiceUrl
     */
    public function __construct()
    {
        //$this->url = $weatherServiceUrl;
    }

    /**
     * @param $city
     * @return mixed
     */
    public function searchWeather($city) {
        $client = new \GuzzleHttp\Client();
        $response = $client->request(
            'GET',
            'https://api.openweathermap.org/data/2.5/weather?q='.$city.'&APPID=d9d0f0b1a9ab275027bc4bb78e940d9d&units=metric'
        );

        // todo check on error trow exception
        //echo $response->getStatusCode(); // 200
        //echo $response->getHeaderLine('content-type'); // 'application/json; charset=utf8'
        return $response->getBody();

    }
}
